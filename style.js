let ourServices = document.querySelectorAll('.our-services-list-item');
let activeItem = null;
let designPicture = document.querySelectorAll('.web-design-text img');
let designText = document.querySelectorAll('.web-design-text p');
for (let listItem of ourServices) {
    listItem.addEventListener('click', function() {
        if (listItem.classList.contains('active')) {
            return
        }
        if (activeItem) {
            activeItem.classList.remove('active');
        }
        ourServices[0].classList.remove('active'); 
        ourServices[0].style.borderLeft = '1px solid rgba(237, 239, 239, 1)';   
        listItem.classList.add('active');
        activeItem = listItem;
        if (activeItem.dataset.tab === 'web') {
            for (let picture of designPicture) {
                if (picture.dataset.tab === 'web') {
                    picture.hidden = false;
                }
                else {
                    picture.hidden = true;
                }
            }
            for (let text of designText) {
                if (text.dataset.tab === 'web') {
                    text.hidden = false;
                }
                else {
                    text.hidden = true;
                }
            }
        }
        else if (activeItem.dataset.tab === 'graphic') {
            for (let picture of designPicture) {
                if (picture.dataset.tab === 'graphic') {
                    picture.hidden = false;
                }
                else {
                    picture.hidden = true;
                }
            }
            for (let text of designText) {
                if (text.dataset.tab === 'graphic') {
                    text.hidden = false;
                }
                else {
                    text.hidden = true;
                }
            }
        }
        else if (activeItem.dataset.tab === 'onlineSupport') {
            for (let picture of designPicture) {
                if (picture.dataset.tab === 'onlineSupport') {
                    picture.hidden = false;
                }
                else {
                    picture.hidden = true;
                }
            }
            for (let text of designText) {
                if (text.dataset.tab === 'onlineSupport') {
                    text.hidden = false;
                }
                else {
                    text.hidden = true;
                }
            }
        }
        else if (activeItem.dataset.tab === 'app') {
            for (let picture of designPicture) {
                if (picture.dataset.tab === 'app') {
                    picture.hidden = false;
                }
                else {
                    picture.hidden = true;
                }
            }
            for (let text of designText) {
                if (text.dataset.tab === 'app') {
                    text.hidden = false;
                }
                else {
                    text.hidden = true;
                }
            }
        }
        else if (activeItem.dataset.tab === 'marketing') {
            for (let picture of designPicture) {
                if (picture.dataset.tab === 'marketing') {
                    picture.hidden = false;
                }
                else {
                    picture.hidden = true;
                }
            }
            for (let text of designText) {
                if (text.dataset.tab === 'marketing') {
                    text.hidden = false;
                }
                else {
                    text.hidden = true;
                }
            }
        }
        else if (activeItem.dataset.tab === 'seoService') {
            for (let picture of designPicture) {
                if (picture.dataset.tab === 'seoService') {
                    picture.hidden = false;
                }
                else {
                    picture.hidden = true;
                }
            }
            for (let text of designText) {
                if (text.dataset.tab === 'seoService') {
                    text.hidden = false;
                }
                else {
                    text.hidden = true;
                }
            }
        }
    })
}
let loadMorePicturesBtn = document.getElementById('load-more-pictures');
console.log(loadMorePicturesBtn);
let amazingWorkGrid = document.querySelector('.amazing-work-grid');
console.log(amazingWorkGrid);
loadMorePicturesBtn.addEventListener('click', function() {
    for (let i = 1; i<=12; i++) {
        let morePictures = document.createElement('div');
        morePictures.classList.add('amazing-work-grid-item');
        let imageSource = document.createElement('img');
        imageSource.src = `img/graphic_design/graphic-design${i}.jpg`;
        morePictures.append(imageSource);
        amazingWorkGrid.append(morePictures);
    }
    loadMorePicturesBtn.hidden = true;
})
let amazingWorkTabs = document.querySelectorAll('.amazing-work-header ul li');
console.log(amazingWorkTabs);
let activeAmazingWorkTab = amazingWorkTabs[0];
let amazingWorkGridFirstItem = document.querySelector('.amazing-work-grid-item.item-1');
let tabAllWasClicked = false;
amazingWorkTabs.forEach(function(tab){
    tab.addEventListener('click', function() {
        if (activeAmazingWorkTab) {
            activeAmazingWorkTab.classList.remove('active');
        }
        tab.classList.add('active');
        activeAmazingWorkTab = tab;
        if (tab.dataset.tab === 'all' && !tabAllWasClicked) {
            tabAllWasClicked = true;       
            for (let child of amazingWorkGrid.children) {
                child.hidden = true;
            }
            amazingWorkGridFirstItem.hidden = false;
            for (let i = 1; i<=11; i++) {
                let morePictures = document.createElement('div');
                morePictures.classList.add('amazing-work-grid-item');
                let imageSource = document.createElement('img');
                imageSource.src = `img/amazing_work_example_${i}.png`;
                morePictures.append(imageSource);
                amazingWorkGrid.append(morePictures);
            }
            for (let i = 1; i<=12; i++) {
                let morePictures = document.createElement('div');
                morePictures.classList.add('amazing-work-grid-item');
                let imageSource = document.createElement('img');
                imageSource.src = `img/graphic_design/graphic-design${i}.jpg`;
                morePictures.append(imageSource);
                amazingWorkGrid.append(morePictures);
            }
             loadMorePicturesBtn.hidden = true;
        }
            else if (tab.dataset.tab === 'graphic') {
            tabAllWasClicked = false;
            for (let child of amazingWorkGrid.children) {
                child.hidden = true;
            }
            amazingWorkGridFirstItem.hidden = false;
            for (let pictureSource of IMAGECATEGORY['graphic']) {
                let categoryPictures = document.createElement('div');
                categoryPictures.classList.add('amazing-work-grid-item');
                let image = document.createElement('img');
                image.src = pictureSource;
                categoryPictures.append(image);
                amazingWorkGridFirstItem.after(categoryPictures);
            }
            loadMorePicturesBtn.hidden = true;
        }
        else if (tab.dataset.tab === 'web') {
            tabAllWasClicked = false;
            for (let child of amazingWorkGrid.children) {
                child.hidden = true;
            }
            amazingWorkGridFirstItem.hidden = false;
            for (let pictureSource of IMAGECATEGORY['web']) {
                let categoryPictures = document.createElement('div');
                categoryPictures.classList.add('amazing-work-grid-item');
                let image = document.createElement('img');
                image.src = pictureSource;
                categoryPictures.append(image);
                amazingWorkGridFirstItem.after(categoryPictures);
            }
             loadMorePicturesBtn.hidden = true;
        }
        else if (tab.dataset.tab === 'landing') {
            tabAllWasClicked = false;
            for (let child of amazingWorkGrid.children) {
                child.hidden = true;
            }
            amazingWorkGridFirstItem.hidden = false;
            for (let pictureSource of IMAGECATEGORY['landing']) {
                let categoryPictures = document.createElement('div');
                categoryPictures.classList.add('amazing-work-grid-item');
                let image = document.createElement('img');
                image.src = pictureSource;
                categoryPictures.append(image);
                amazingWorkGridFirstItem.after(categoryPictures);
            }
             loadMorePicturesBtn.hidden = true;
        }
        else if (tab.dataset.tab === 'wordpress') {
            tabAllWasClicked = false;
            for (let child of amazingWorkGrid.children) {
                child.hidden = true;
            }
            amazingWorkGridFirstItem.hidden = false;
            for (let pictureSource of IMAGECATEGORY['wordpress']) {
                let categoryPictures = document.createElement('div');
                categoryPictures.classList.add('amazing-work-grid-item');
                let image = document.createElement('img');
                image.src = pictureSource;
                categoryPictures.append(image);
                amazingWorkGridFirstItem.after(categoryPictures);
            }
             loadMorePicturesBtn.hidden = true;
        }
    })
})
const IMAGECATEGORY = {
    graphic: ['image/graphic design/graphic-design1.jpg', 'image/graphic design/graphic-design2.jpg', 'image/graphic design/graphic-design3.jpg', 'image/graphic design/graphic-design5.jpg', 'image/graphic design/graphic-design4.jpg', 'image/graphic design/graphic-design6.jpg', 'image/graphic design/graphic-design7.jpg.jpg'],
    web: ["image/web design/web-design1.jpg", "image/web design/web-design2.jpg", "image/web design/web-design4.jpg", "image/web design/web-design3.jpg", "image/web design/web-design6.jpg", "image/web design/web-design5.jpg", "image/web design/web-design7.jpg"],
    landing: ["image/landing page/landing-page1.jpg", "image/landing page/landing-page2.jpg", "image/landing page/landing-page3.jpg", 'image/landing page/landing-page4.jpg', 'image/landing page/landing-page6.jpg', "image/landing page/landing-page5.jpg", "image/landing page/landing-page7.jpg"],
    wordpress: ['image/wordpress/wordpress1.jpg', 'image/wordpress/wordpress2.jpg', 'image/wordpress/wordpress3.jpg', 'image/wordpress/wordpress4.jpg', 'image/wordpress/wordpress6.jpg', 'image/wordpress/wordpress5.jpg', 'image/wordpress/wordpress7.jpg'],
} 
let sliderLessSign = document.querySelector('.slider-less-sign');
let sliderMoreSign = document.querySelector('.slider-more-sign');
let photoContainers = document.querySelectorAll('.avatar-wrapper-slider');
let bigAvatar = document.querySelector('.photo-of-a-person');
let feedback = document.querySelector('.section-feedbacks-text');
let nameOfPeople = document.querySelector('.name-of-people');
let occupationOfPeople = document.querySelector('.occupation-of-people');
let peopleFeedbacksText = document.querySelectorAll('.people-feedbacks-hidden-text');
let peopleNamesText = document.querySelectorAll('.people-names-hidden-text');
let peopleOccupationsText = document.querySelectorAll('.people-occupations-hidden-text');
console.log(peopleFeedbacksText);
let num = 2;
for (let photo of photoContainers) {
   if (photo.children[0].getAttribute('alt') === 'hasan_ali_photo') {
    photo.style.position = 'relative';
    photo.style.top = '-9px';
   }
}
let activeAvatar = photoContainers[2];
sliderLessSign.addEventListener('click', function(){
    num--;
    if (num < 0) {
        num = photoContainers.length - 1;
    }
    if (activeAvatar) {
        activeAvatar.style.position = '';
        activeAvatar.style.top = '';
    }

    if (photoContainers[num].dataset.personName === "Lara") {
        for (let review of peopleFeedbacksText) {
            if (review.dataset.personName === "Lara") {
                feedback.textContent = review.textContent;
            }
        }
        for (let name of peopleNamesText) {
            if (name.dataset.personName === "Lara") {
                nameOfPeople.textContent = name.textContent;
            }
        }
        for (let occupation of peopleOccupationsText) {
            if (occupation.dataset.personName === "Lara") {
                occupationOfPeople.textContent = occupation.textContent;
            }
        }
    }
    else if (photoContainers[num].dataset.personName === "Mark") {
        for (let review of peopleFeedbacksText) {
            if (review.dataset.personName === "Mark") {
                feedback.textContent = review.textContent;
            }
        }
        for (let name of peopleNamesText) {
            if (name.dataset.personName === "Mark") {
                nameOfPeople.textContent = name.textContent;
            }
        }
        for (let occupation of peopleOccupationsText) {
            if (occupation.dataset.personName === "Mark") {
                occupationOfPeople.textContent = occupation.textContent;
            }
        }
    }
    else if (photoContainers[num].dataset.personName === "Alex") {
        for (let review of peopleFeedbacksText) {
            if (review.dataset.personName === "Alex") {
                feedback.textContent = review.textContent;
            }
        }
        for (let name of peopleNamesText) {
            if (name.dataset.personName === "Alex") {
                nameOfPeople.textContent = name.textContent;
            }
        }
        for (let occupation of peopleOccupationsText) {
            if (occupation.dataset.personName === "Alex") {
                occupationOfPeople.textContent = occupation.textContent;
            }
        }
    }
    else if (photoContainers[num].dataset.personName === "Hasan") {
        for (let review of peopleFeedbacksText) {
            if (review.dataset.personName === "Hasan") {
                feedback.textContent = review.textContent;
            }
        }
        for (let name of peopleNamesText) {
            if (name.dataset.personName === "Hasan") {
                nameOfPeople.textContent = name.textContent;
            }
        }
        for (let occupation of peopleOccupationsText) {
            if (occupation.dataset.personName === "Hasan") {
                occupationOfPeople.textContent = occupation.textContent;
            }
        }
    }
        photoContainers[num].style.transition = "all 3s ease";
    photoContainers[num].style.position = 'relative';
    photoContainers[num].style.top = '-9px';
    bigAvatar.src = photoContainers[num].children[0].getAttribute('src');
    activeAvatar =  photoContainers[num];
})
sliderMoreSign.addEventListener('click', function(){
    num++;
    if (num >= photoContainers.length) {
        num = 0;
    }
    if (activeAvatar) {
        activeAvatar.style.position = '';
        activeAvatar.style.top = '';
    }
    if (photoContainers[num].dataset.personName === "Mark") {
        for (let review of peopleFeedbacksText) {
            if (review.dataset.personName === "Mark") {
                feedback.textContent = review.textContent;
            }
        }
        for (let name of peopleNamesText) {
            if (name.dataset.personName === "Mark") {
                nameOfPeople.textContent = name.textContent;
            }
        }
        for (let occupation of peopleOccupationsText) {
            if (occupation.dataset.personName === "Mark") {
                occupationOfPeople.textContent = occupation.textContent;
            }
        }
    }
    else if (photoContainers[num].dataset.personName === "Maks") {
        for (let review of peopleFeedbacksText) {
            if (review.dataset.personName === "Maks") {
                feedback.textContent = review.textContent;
            }
        }
        for (let name of peopleNamesText) {
            if (name.dataset.personName === "Maks") {
                nameOfPeople.textContent = name.textContent;
            }
        }
        for (let occupation of peopleOccupationsText) {
            if (occupation.dataset.personName === "Maks") {
                occupationOfPeople.textContent = occupation.textContent;
            }
        }
    }
    else if (photoContainers[num].dataset.personName === "Alex") {
        for (let review of peopleFeedbacksText) {
            if (review.dataset.personName === "Alex") {
                feedback.textContent = review.textContent;
            }
        }
        for (let name of peopleNamesText) {
            if (name.dataset.personName === "Alex") {
                nameOfPeople.textContent = name.textContent;
            }
        }
        for (let occupation of peopleOccupationsText) {
            if (occupation.dataset.personName === "Alex") {
                occupationOfPeople.textContent = occupation.textContent;
            }
        }
    }
    else if (photoContainers[num].dataset.personName === "Nazar") {
        for (let review of peopleFeedbacksText) {
            if (review.dataset.personName === "Nazar") {
                feedback.textContent = review.textContent;
            }
        }
        for (let name of peopleNamesText) {
            if (name.dataset.personName === "Nazar") {
                nameOfPeople.textContent = name.textContent;
            }
        }
        for (let occupation of peopleOccupationsText) {
            if (occupation.dataset.personName === "Nazar") {
                occupationOfPeople.textContent = occupation.textContent;
            }
        }
    }
    photoContainers[num].style.position = 'relative';
    photoContainers[num].style.top = '-9px';
    bigAvatar.src = photoContainers[num].children[0].getAttribute('src');
    activeAvatar =  photoContainers[num];
})
for (let avatar of photoContainers) {
    avatar.addEventListener('click', function(){
        if (activeAvatar) {
            activeAvatar.style.position = '';
            activeAvatar.style.top = '';
        }
        if (avatar.dataset.personName === "Mark") {
            for (let review of peopleFeedbacksText) {
                if (review.dataset.personName === "Mark") {
                    feedback.textContent = review.textContent;
                }
            }
            for (let name of peopleNamesText) {
                if (name.dataset.personName === "Mark") {
                    nameOfPeople.textContent = name.textContent;
                }
            }
            for (let occupation of peopleOccupationsText) {
                if (occupation.dataset.personName === "Mark") {
                    occupationOfPeople.textContent = occupation.textContent;
                }
            }
        }
        else if (avatar.dataset.personName === "Maks") {
            for (let review of peopleFeedbacksText) {
                if (review.dataset.personName === "Maks") {
                    feedback.textContent = review.textContent;
                }
            }
            for (let name of peopleNamesText) {
                if (name.dataset.personName === "Maks") {
                    nameOfPeople.textContent = name.textContent;
                }
            }
            for (let occupation of peopleOccupationsText) {
                if (occupation.dataset.personName === "Maks") {
                    occupationOfPeople.textContent = occupation.textContent;
                }
            }
        }
        else if (avatar.dataset.personName === "Alex") {
            for (let review of peopleFeedbacksText) {
                if (review.dataset.personName === "Alex") {
                    feedback.textContent = review.textContent;
                }
            }
            for (let name of peopleNamesText) {
                if (name.dataset.personName === "Alex") {
                    nameOfPeople.textContent = name.textContent;
                }
            }
            for (let occupation of peopleOccupationsText) {
                if (occupation.dataset.personName === "Alex") {
                    occupationOfPeople.textContent = occupation.textContent;
                }
            }
        }
        else if (avatar.dataset.personName === "Nazar") {
            for (let review of peopleFeedbacksText) {
                if (review.dataset.personName === "Nazar") {
                    feedback.textContent = review.textContent;
                }
            }
            for (let name of peopleNamesText) {
                if (name.dataset.personName === "Nazar") {
                    nameOfPeople.textContent = name.textContent;
                }
            }
            for (let occupation of peopleOccupationsText) {
                if (occupation.dataset.personName === "Nazar") {
                    occupationOfPeople.textContent = occupation.textContent;
                }
            }
        }
        avatar.style.position = 'relative';
        avatar.style.top = '-9px';
        bigAvatar.src = avatar.children[0].getAttribute('src');
        activeAvatar =  avatar;
    })
}